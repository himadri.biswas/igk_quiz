import { Link } from "react-router-dom";
function Question1(){
    return(
    <div className="flex items-center justify-center container mx-auto bg-green-200 w-1/2 h-96 my-36">
        <div className="flex-none w-18 h-14 ">
        <Link to="/" className="bg-stone-50 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l">
            Prev
        </Link>
        </div>
        <div className="grow flex justify-center h-14">
          Body[Q1]
        </div>
        <div className="flex-none w-18 h-14">
        <Link to="/question2" className="bg-stone-50 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-r">
            Next
        </Link>
        </div>
      </div>
    );
}
export default Question1