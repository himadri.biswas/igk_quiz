import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from "react-router-dom";

function Home(){

    return(
        <div className="flex items-center justify-center container mx-auto bg-green-200 w-1/2 h-96 my-36">
         
        <Link to="/question1" className="px-4 py-2 border-solid border-2 border-indigo-600 rounded-md shadow-sm">Start Quiz</Link>

        </div>
        
    );

}
export default Home